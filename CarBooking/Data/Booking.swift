//
//  Booking.swift
//  CarBooking
//
//  Created by Milan Kazarka on 5/5/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class Booking: NSObject {
    
    var date : Date? = nil
    var car : Car? = nil
    var duration = Int(0)
    
    init(withDate:Date, car:Car, duration:Int) {
        self.date = withDate
        self.car = car
        self.duration = duration
    }
}
