//
//  Bookings.swift
//  CarBooking
//
//  Created by Milan Kazarka on 5/5/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class Bookings: NSObject {

    public var bookings: Array<NSMutableDictionary> = Array()
    
    static let sharedInstance = Bookings()

    func reload( ) {
        let defaults = UserDefaults.standard
        if let bookingsCopy = defaults.object(forKey: "Bookings") as? Array<NSMutableDictionary> {
            bookings = bookingsCopy
        }
    }
    
    func save( ) {
        let defaults = UserDefaults.standard
        defaults.set(bookings, forKey: "Bookings")
        defaults.synchronize()
        reload()
    }
    
    func addBooking( booking:Booking ) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy hh:00 a"
        
        let dict = NSMutableDictionary()
        dict.setValue("\(dateFormatter.string(from: booking.date!))", forKey:"date")
        dict.setValue(booking.car!.name, forKey: "name")
        dict.setValue(NSNumber(value:booking.duration), forKey: "duration")
        dict.setValue(booking.car?.id, forKey: "id")
        
        bookings.insert(dict, at: 0)
        save()
    }
}
