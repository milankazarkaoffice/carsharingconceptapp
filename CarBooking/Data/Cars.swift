//
//  Cars.swift
//  CarBooking
//
//  Created by Milan Kazarka on 5/3/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class Cars: NSObject {

    public var cars: Array<Car> = Array()
    
    static let sharedInstance = Cars()
    
    /** simply reload all data, standard URL query, data access synced
     */
    func reload( ) {
        
        guard let url = URL(string: Common.urlListCars) else {
            print("couldn't create URL for call")
            return
        }
        let theRequest = URLRequest(url: url)
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: theRequest, completionHandler: { (data, response, error) in
            print("response(\(response)) error(\(error))")
            if let data = data as Data? {
                do {
                    let parsedData = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments)
                    
                    DispatchQueue.main.async {
                        
                        let lockQueue = DispatchQueue(label: Common.locknameData)
                        lockQueue.sync() {
                            
                            self.cars.removeAll()
                            self.parseCarsResponse(dataTree:parsedData)
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Common.notifDataChanged), object: nil)
                        }
                        
                    }
                }
                catch let error as NSError {
                    print("Details of JSON parsing error:\n \(error)")
                }
            }
        })
        
        task.resume()
    }
    
    func parseCarsResponse( dataTree: Any ) {
        
        if let carsArray = dataTree as? NSArray {
            for i in (0..<carsArray.count) {
                if let carDef = carsArray.object(at: i) as? NSDictionary {
                    let car = Car(withDictionary:carDef)
                    self.cars.append(car)
                }
            }
            
            let sortedCars = self.cars.sorted(by: { (car1, car2) -> Bool in
                return car1.name! < car2.name!
            })
            
            self.cars = sortedCars
        }
    }
    
    func carAtIndex( index: Int ) -> Car? {
        var car : Car? = nil
        let lockQueue = DispatchQueue(label: Common.locknameData)
        lockQueue.sync() {
            let nocopy = self.cars[index]
            car = nocopy.copy() as? Car
        }
        return car
    }
}
