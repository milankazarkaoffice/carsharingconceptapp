//
//  Car.swift
//  CarBooking
//
//  Created by Milan Kazarka on 5/3/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class Car: NSObject, NSCopying {

    var id = NSNumber.init(value: 0)
    var name = String("")
    var shortDescription = String("")
    
    init( withDictionary: NSDictionary ) {
        super.init()
        if withDictionary.object(forKey: "id") != nil {
            id = withDictionary.object(forKey: "id") as! NSNumber
        }
        if withDictionary.object(forKey: "name") != nil {
            name = withDictionary.object(forKey: "name") as? String
        }
        if withDictionary.object(forKey: "shortDescription") != nil {
            shortDescription = withDictionary.object(forKey: "shortDescription") as? String
        }
    }
    
    init( id: NSNumber, name: String, shortDescription: String ) {
        self.id = id
        self.name = name
        self.shortDescription = shortDescription
    }
    
    override init() {
        
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = Car(id:self.id,name:self.name!,shortDescription:self.shortDescription!)
        return copy
    }
    
}
