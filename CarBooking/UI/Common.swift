//
//  Common.swift
//  CarBooking
//
//  Created by Milan Kazarka on 5/3/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

struct Common {
    static let colorPrimary = UIColor(red:172.0/255.0, green:207.0/255.0, blue:204.0/255.0, alpha:1.0)
    static let colorSecondary = UIColor(red:89.0/255.0, green:82.0/255.0, blue:65.0, alpha:1.0)
    static let colorAccent = UIColor(red:138.0/255.0, green:9.0/255.0, blue:23.0/255.0, alpha:1.0)
    
    static let cornerRadius = CGFloat(5.0)
    static let offset = Common.cornerRadius
    
    static let urlListCars : String = "http://www.sebastianf.net/intive/api/v1/cars.json"
    static let urlCarDetailBasis : String = "http://www.sebastianf.net/intive/api/v1/cars"
    static let urlCarImagesBasis : String = "http://sebastianf.net/intive/api"
    
    static let notifDataChanged : String = "notifDataChanged"
    
    static let locknameData : String = "com.dataLock"
}
