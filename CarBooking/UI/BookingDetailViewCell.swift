//
//  BookingDetailViewCell.swift
//  CarBooking
//
//  Created by Milan Kazarka on 5/5/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class BookingDetailViewCell: CarBookingCell {

    static let cellIdentifier = "BookingDetailViewCell"
    
    var contentHolder : UIView = UIView()
    
    var nameLabel : UILabel = UILabel()
    var durationLabel : UILabel = UILabel()
    var dateLabel : UILabel = UILabel()
    
    var animatedArrow : AnimatedArrowView = AnimatedArrowView()
    
    func loadBooking( bookingDef: NSMutableDictionary ) {
        if let name = bookingDef.object(forKey: "name") as? String {
            nameLabel.text = name
        }
        if let date = bookingDef.object(forKey: "date") as? String {
            dateLabel.text = date
        }
        if let duration = bookingDef.object(forKey: "duration") as? NSNumber {
            durationLabel.text = "\(duration) d"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        contentHolder.backgroundColor = UIColor.white
        contentHolder.layer.cornerRadius = CarListTableViewCell.contentHolderCornerRadius
        
        contentHolder.addSubview(self.nameLabel)
        contentHolder.addSubview(self.durationLabel)
        contentHolder.addSubview(self.dateLabel)
        contentHolder.addSubview(self.animatedArrow)
        
        self.contentView.addSubview(contentHolder)
        self.contentView.backgroundColor = UIColor(white:0.95, alpha:1.0)
        
        let deadlineTime = DispatchTime.now() + .seconds(1)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            self.animatedArrow.animate()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let offset = CarListTableViewCell.contentHolderOffset
        contentHolder.frame = CGRect(x:offset,y:offset,width:self.contentView.frame.size.width-(2.0*offset),height:self.contentView.frame.size.height-offset)
        nameLabel.frame = CGRect(x:offset,y:offset,width:self.contentView.frame.size.width-(2.0*offset)-self.contentView.frame.size.height,height:(self.contentView.frame.size.height-(2.0*offset))/2.0)
        dateLabel.frame = CGRect(x:offset,y:nameLabel.frame.origin.y+nameLabel.frame.size.height,width:(self.contentView.frame.size.width-(2.0*offset)-self.contentView.frame.size.height)*(2.0/3.0),height:(self.contentView.frame.size.height-(2.0*offset))/2.0)
        durationLabel.frame = CGRect(x:dateLabel.frame.origin.x+dateLabel.frame.size.width,y:nameLabel.frame.origin.y+nameLabel.frame.size.height,width:(self.contentView.frame.size.width-(2.0*offset)-self.contentView.frame.size.height)*(1.0/3.0),height:(self.contentView.frame.size.height-(2.0*offset))/2.0)
        animatedArrow.frame = CGRect(x:self.contentView.frame.size.width-((self.contentView.frame.size.height/6.0)*3.0),y:self.contentView.frame.size.height/3,width:self.contentView.frame.size.height/3,height:self.contentView.frame.size.height/3.0)
    }
}
