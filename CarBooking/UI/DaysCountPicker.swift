//
//  DaysCountPicker.swift
//  CarBooking
//
//  Created by Milan Kazarka on 5/5/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class DaysCountPicker: UIView {

    var buttonDownBorder = UIView()
    var buttonUpBorder = UIView()
    
    var buttonDown = UIButton()
    var buttonUp = UIButton()
    
    var infoLabel = UILabel()
    
    var days = Int(1)
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }

    override init(frame:CGRect) {
        super.init(frame:frame)
        
        self.addSubview(buttonDownBorder)
        self.addSubview(buttonUpBorder)
        self.addSubview(buttonDown)
        self.addSubview(buttonUp)
        self.addSubview(infoLabel)
        
        infoLabel.textAlignment = .center
        infoLabel.text = "1"
        
        buttonDownBorder.layer.cornerRadius = Common.cornerRadius
        buttonUpBorder.layer.cornerRadius = Common.cornerRadius
        
        buttonDownBorder.backgroundColor = Common.colorAccent
        buttonUpBorder.backgroundColor = Common.colorAccent
        
        buttonDown.layer.cornerRadius = Common.cornerRadius-1
        buttonUp.layer.cornerRadius = Common.cornerRadius-1
        
        buttonDown.setTitleColor(Common.colorAccent, for: .normal)
        buttonUp.setTitleColor(Common.colorAccent, for: .normal)
        
        buttonDown.backgroundColor = UIColor(white:1.0,alpha:0.9)
        buttonUp.backgroundColor = UIColor(white:1.0,alpha:0.9)
        
        buttonDown.setTitle("-", for: .normal)
        buttonUp.setTitle("+", for: .normal)
        
        buttonDown.addTarget(self,
                          action: #selector(onDown),
                          for: .touchUpInside
        )
        buttonUp.addTarget(self,
                             action: #selector(onUp),
                             for: .touchUpInside
        )
    }
    
    func onUpdate() {
        infoLabel.text = "\(days)"
    }
    
    func onDown() {
        if days != 1 {
            days-=1
            onUpdate()
        }
    }
    
    func onUp() {
        if days != 7 {
            days+=1
            onUpdate()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            
            super.frame = newFrame
            
            let buttonWidth = CGFloat(100.0)
            let borderWidth = CGFloat(1.0)
            let offset = Common.offset
            
            var downBorderFrame = CGRect(x:offset,y:offset,width:buttonWidth,height:newFrame.size.height-(2.0*offset))
            var upBorderFrame = CGRect(x:newFrame.size.width-offset-buttonWidth,y:offset,width:buttonWidth,height:newFrame.size.height-(2.0*offset))
            
            buttonDownBorder.frame = downBorderFrame
            buttonUpBorder.frame = upBorderFrame
            
            downBorderFrame.origin.x+=borderWidth
            downBorderFrame.origin.y+=borderWidth
            downBorderFrame.size.width-=2.0*borderWidth
            downBorderFrame.size.height-=2.0*borderWidth
            
            upBorderFrame.origin.x+=borderWidth
            upBorderFrame.origin.y+=borderWidth
            upBorderFrame.size.width-=2.0*borderWidth
            upBorderFrame.size.height-=2.0*borderWidth
            
            buttonDown.frame = downBorderFrame
            buttonUp.frame = upBorderFrame
            infoLabel.frame = CGRect(x:buttonDown.frame.origin.x+buttonDown.frame.size.width,y:offset,width:buttonUp.frame.origin.x-buttonDown.frame.origin.x-buttonDown.frame.size.width,height:newFrame.size.height-(2.0*offset))
        }
    }
    
}
