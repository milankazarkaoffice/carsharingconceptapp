//
//  CarBookingCell.swift
//  CarBooking
//
//  Created by Milan Kazarka on 5/5/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class CarBookingCell: UITableViewCell {

    static let cellHeight = CGFloat(90.0)
    static let contentHolderOffset = CGFloat(3.0)
    static let contentHolderCornerRadius = CGFloat(3.0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
