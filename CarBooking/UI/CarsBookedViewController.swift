//
//  CarsBookedViewController.swift
//  CarBooking
//
//  Created by Milan Kazarka on 5/3/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class CarsBookedViewController: CommonViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView : UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView?.register(CarListTableViewCell.self, forCellReuseIdentifier: BookingDetailViewCell.cellIdentifier)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Bookings.sharedInstance.bookings.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CarListTableViewCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : BookingDetailViewCell! = nil
        cell = BookingDetailViewCell(style:UITableViewCellStyle.default, reuseIdentifier: BookingDetailViewCell.cellIdentifier)
        cell.loadBooking(bookingDef: Bookings.sharedInstance.bookings[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"BookingDetailsViewController") as! BookingDetailsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        viewController.loadBookingDef(bookingDef: Bookings.sharedInstance.bookings[indexPath.row])
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
