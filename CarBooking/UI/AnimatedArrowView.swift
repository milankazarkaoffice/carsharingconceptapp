//
//  AnimatedArrowView.swift
//  CarBooking
//
//  Created by Milan Kazarka on 5/4/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class AnimatedArrowView: UIView {

    var upperView : UIView = UIView()
    var lowerView : UIView = UIView()

    var layedOut : Bool = false
    
    convenience init() {
        self.init(frame: CGRect.zero)
        self.autoresizingMask = UIViewAutoresizing.flexibleWidth
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        
        upperView.backgroundColor = Common.colorSecondary
        upperView.layer.cornerRadius = 2.0
        upperView.alpha = 0.6
        lowerView.backgroundColor = Common.colorAccent
        lowerView.layer.cornerRadius = 2.0
        lowerView.alpha = 0.6
                
        self.addSubview(upperView)
        self.addSubview(lowerView)
    }
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            
            super.frame = newFrame
            
            if newFrame.size.height>0 && !layedOut {
                let thick = CGFloat(4)
                upperView.frame = CGRect(x:0.0, y:(self.frame.size.height/4.0)+(thick/2.0), width:self.frame.size.width, height:thick)
                lowerView.frame = CGRect(x:0.0, y:((self.frame.size.height/4.0)*3.0)-(thick/2.0), width:self.frame.size.width, height:thick)
                layedOut=true
            }
        }
    }
    
    func animate( ) {
        
        UIView.animate(
            withDuration: 0.5,
            delay: 0,
            options: .beginFromCurrentState,
            animations: { () -> Void in
                
                self.upperView.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI_4/2.0))
                self.lowerView.transform = CGAffineTransform(rotationAngle: CGFloat(-M_PI_4/2.0))
        }) { (completed:Bool) -> Void in
            
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
