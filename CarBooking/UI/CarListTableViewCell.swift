//
//  CarListTableViewCell.swift
//  CarBooking
//
//  Created by Milan Kazarka on 5/3/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class CarListTableViewCell: CarBookingCell {

    static let cellIdentifier = "CarListTableViewCell"
    
    var contentHolder : UIView = UIView()
    var nameLabel : UILabel = UILabel()
    var shortDescriptionLabel : UILabel = UILabel()
    
    var animatedArrow : AnimatedArrowView = AnimatedArrowView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        shortDescriptionLabel.numberOfLines = 2
        shortDescriptionLabel.textColor = UIColor.gray
        
        self.selectionStyle = .none
        contentHolder.backgroundColor = UIColor.white
        contentHolder.layer.cornerRadius = CarListTableViewCell.contentHolderCornerRadius
        contentHolder.addSubview(self.nameLabel)
        contentHolder.addSubview(self.shortDescriptionLabel)
        contentHolder.addSubview(self.animatedArrow)
        self.contentView.addSubview(contentHolder)
        self.contentView.backgroundColor = UIColor(white:0.95, alpha:1.0)
        
        let deadlineTime = DispatchTime.now() + .seconds(1)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            self.animatedArrow.animate()
        }
    }
    
    func loadCar( car: Car ) {
        self.nameLabel.text = car.name
        self.shortDescriptionLabel.text = car.shortDescription
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let offset = CarListTableViewCell.contentHolderOffset
        contentHolder.frame = CGRect(x:offset,y:offset,width:self.contentView.frame.size.width-(2.0*offset),height:self.contentView.frame.size.height-offset)
        nameLabel.frame = CGRect(x:offset,y:offset,width:self.contentView.frame.size.width-(2.0*offset)-self.contentView.frame.size.height,height:(self.contentView.frame.size.height-(2.0*offset))/2.0)
        shortDescriptionLabel.frame = CGRect(x:offset,y:nameLabel.frame.origin.y+nameLabel.frame.size.height,width:self.contentView.frame.size.width-(2.0*offset)-self.contentView.frame.size.height,height:(self.contentView.frame.size.height-(2.0*offset))/2.0)
        animatedArrow.frame = CGRect(x:self.contentView.frame.size.width-((self.contentView.frame.size.height/6.0)*3.0),y:self.contentView.frame.size.height/3,width:self.contentView.frame.size.height/3,height:self.contentView.frame.size.height/3.0)
    }

}
