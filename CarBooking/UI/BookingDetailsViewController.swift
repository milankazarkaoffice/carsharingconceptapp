//
//  BookingDetailsViewController.swift
//  CarBooking
//
//  Created by Milan Kazarka on 5/3/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class BookingDetailsViewController: CommonViewController {

    let car : Car = Car()
    
    var scrollView = UIScrollView()
    var carNameLbl = UILabel()
    var dateLbl = UILabel()
    var durationLbl = UILabel()
    var carDetailBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = L.IN(ins: "Booking details >_<")
        
        self.view.addSubview(scrollView)
        scrollView.addSubview(carDetailBtn)
        scrollView.addSubview(carNameLbl)
        scrollView.addSubview(dateLbl)
        scrollView.addSubview(durationLbl)
        
        carDetailBtn.layer.cornerRadius = Common.cornerRadius
        carDetailBtn.setTitleColor(UIColor.white, for: .normal)
        carDetailBtn.setTitle("Car details", for: .normal)
        carDetailBtn.backgroundColor = Common.colorSecondary
        
        carDetailBtn.addTarget(self,
                          action: #selector(showCarDetail),
                          for: .touchUpInside
        )
        
    }
    
    func showCarDetail( ) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"CarDetailsViewController") as! CarDetailsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        viewController.loadCar(car: car)
    }
    
    func loadBookingDef(bookingDef:NSMutableDictionary) {
        if let carid = bookingDef.object(forKey: "id") as? NSNumber {
            car.id = carid
        }
        if let date = bookingDef.object(forKey: "date") as? String {
            dateLbl.text = date
        }
        if let name = bookingDef.object(forKey: "name") as? String {
            carNameLbl.text = name
        }
        if let duration = bookingDef.object(forKey: "duration") as? NSNumber {
            durationLbl.text = "duration: \(duration) days"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var ypos = CGFloat(0.0)
        let lineHeight = CGFloat(60.0)
        let offset = Common.offset
        
        scrollView.frame = CGRect(x:0.0,y:0.0,width:self.view.frame.size.width,height:self.view.frame.size.height)
        
        carNameLbl.frame = CGRect(x:offset,y:ypos,width:self.view.frame.size.width-(2.0*offset),height:lineHeight)
        ypos+=carNameLbl.frame.size.height
        dateLbl.frame = CGRect(x:offset,y:ypos,width:self.view.frame.size.width-(2.0*offset),height:lineHeight)
        ypos+=dateLbl.frame.size.height
        durationLbl.frame = CGRect(x:offset,y:ypos,width:self.view.frame.size.width-(2.0*offset),height:lineHeight)
        ypos+=durationLbl.frame.size.height
        carDetailBtn.frame = CGRect(x:offset,y:ypos,width:self.view.frame.size.width-(2.0*offset),height:lineHeight)
        ypos+=carDetailBtn.frame.size.height
    }

}
