//
//  CarDetailsViewController.swift
//  CarBooking
//
//  Created by Milan Kazarka on 5/3/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit
import DatePickerDialog

class CarDetailsViewController: CommonViewController {

    var scrollView = UIScrollView()
    var loadingImageLabel = UILabel()
    var carImage = UIImageView()
    var nameLabel = UILabel()
    var shortLabel = UILabel()
    var descriptionLabel = LabelWithInsets()
    
    var dateBtn = UIButton()
    var bookBtn = UIButton()
    var moreBtn = UIButton()
    var daysCountPicker = DaysCountPicker()
    
    var detailShown = false
    
    var selectedDate = Date()
    
    var loadedCar : Car? = nil
    
    func loadCar(car:Car) {
        
        loadedCar = car
        
        guard let url = URL(string: "\(Common.urlCarDetailBasis)/\(car.id).json") else {
            print("couldn't create URL for call")
            return
        }
        let theRequest = URLRequest(url: url)
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: theRequest, completionHandler: { (data, response, error) in
            print("response(\(response)) error(\(error))")
            if let data = data as Data? {
                do {
                    let parsedData = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments)
                    
                    DispatchQueue.main.async {
                        
                        let lockQueue = DispatchQueue(label: Common.locknameData)
                        lockQueue.sync() {
                            
                            self.parseCarDetailResponse(dataTree: parsedData)
                            
                        }
                    }
                } catch let error as NSError {
                    print("Details of JSON parsing error:\n \(error)")
                }
            }
        })
        task.resume()
    }
    
    func parseCarDetailResponse( dataTree: Any ) {
        if let attributes = dataTree as? NSDictionary {
            print("we got attributes")
            if let name = attributes.object(forKey: "name") as? NSString {
                self.nameLabel.text = name as String
            }
            if let short = attributes.object(forKey: "shortDescription") as? NSString {
                self.shortLabel.text = short as String
            }
            if let description = attributes.object(forKey: "description") as? NSString {
                self.descriptionLabel.text = description as String
                self.moreBtn.isHidden = false
            }
            if let imageAddress = attributes.object(forKey: "image") as? NSString {
                
                let url = URL(string: (Common.urlCarImagesBasis as String)+"/"+(imageAddress as String))
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!)
                    if data != nil {
                        DispatchQueue.main.async {
                            self.loadingImageLabel.removeFromSuperview()
                            self.carImage.image = UIImage(data: data!)
                        }
                    }
                    else
                    {
                        self.loadingImageLabel.text = L.IN(ins:"No image available")
                    }
                }
                
            }
        }
    }
    
    func setDateLabelInitial() {
        let today = Date()
        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
        var components = NSCalendar.current.dateComponents(in: TimeZone.current, from: tomorrow!)
        components.hour = 9
        components.minute = 0
        let tomorrowWithTime = NSCalendar.current.date(from: components)
        
        print("setting initial time")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy hh:00 a"
        print("Dateobj: \(dateFormatter.string(from: tomorrowWithTime!))")
        dateBtn.setTitle("\(dateFormatter.string(from: tomorrowWithTime!))", for: .normal)
        
        selectedDate = tomorrow!
    }
    
    func onSelectDate() {
        print("onSelectDate")
        
        let today = Date()
        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
        var components = NSCalendar.current.dateComponents(in: TimeZone.current, from: tomorrow!)
        components.hour = 9
        components.minute = 0
        let tomorrowWithTime = NSCalendar.current.date(from: components)
        
        DatePickerDialog().show(title: L.IN(ins:"DatePicker"), doneButtonTitle: L.IN(ins:"Done"), cancelButtonTitle: L.IN(ins:"Cancel"), defaultDate: tomorrowWithTime!, datePickerMode: .dateAndTime) {
            (date) -> Void in
            print("date selected (\(date))")
            if date != nil {
                
                let today = Date()
                let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
                if tomorrow!.compare(date!) == ComparisonResult.orderedDescending {
                    self.setDateLabelInitial()
                    
                    let alert = UIAlertController(title: L.IN(ins:"No!"), message: L.IN(ins:"Can set the earliest date of tomorrow 9AM :("), preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: L.IN(ins:"Ok!"), style: UIAlertActionStyle.default, handler:nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    self.selectedDate = tomorrow!
                    
                } else {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
                    print("Dateobj: \(dateFormatter.string(from: date!))")
                    self.dateBtn.setTitle("\(dateFormatter.string(from: date!))", for: .normal)
                    
                    self.selectedDate = date!
                }
            }
        }
    }
    
    func onOk() {
        print("onOk")
    }
    
    func onBook() {
        print("onBook")
        
        let booking = Booking(withDate:selectedDate,car:loadedCar!,duration:daysCountPicker.days)
        Bookings.sharedInstance.addBooking(booking:booking)
        
        let alert = UIAlertController(title: L.IN(ins:"Booked!"), message: L.IN(ins:"You booked a car!"), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: L.IN(ins:"Ok!"), style: UIAlertActionStyle.default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                self.onOk()
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func onMore() {
        print("onMore")
        moreBtn.isHidden = true
        detailShown = true
        rearrangeViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = L.IN(ins: "Car details o_o")
        
        self.view.addSubview(scrollView)
                
        scrollView.addSubview(carImage)
        scrollView.addSubview(loadingImageLabel)
        scrollView.addSubview(descriptionLabel)
        
        carImage.addSubview(nameLabel)
        nameLabel.textColor = UIColor.white
        nameLabel.textAlignment = .center
        nameLabel.backgroundColor = UIColor(white:0.0,alpha:0.35)
        carImage.addSubview(shortLabel)
        shortLabel.textColor = UIColor.white
        shortLabel.textAlignment = .center
        shortLabel.numberOfLines = 2
        shortLabel.backgroundColor = UIColor(white:0.0,alpha:0.35)
        
        descriptionLabel.backgroundColor = UIColor(white:0.0,alpha:1.0)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textColor = UIColor.white
        descriptionLabel.alpha = 0.9
        descriptionLabel.text = L.IN(ins:"Detail")
        scrollView.addSubview(moreBtn)
        moreBtn.layer.cornerRadius = Common.cornerRadius
        moreBtn.setTitle(L.IN(ins:"More"), for: .normal)
        moreBtn.backgroundColor = UIColor(white:1.0,alpha:0.8)
        moreBtn.setTitleColor(UIColor.black, for: .normal)
        moreBtn.isHidden = true
        moreBtn.addTarget(self,
                          action: #selector(onMore),
                          for: .touchUpInside
        )
        scrollView.addSubview(dateBtn)
        scrollView.addSubview(daysCountPicker)
        scrollView.addSubview(bookBtn)
        carImage.backgroundColor = UIColor.lightGray
        loadingImageLabel.text = L.IN(ins:"Loading image")
        loadingImageLabel.textColor = UIColor.white
        loadingImageLabel.backgroundColor = UIColor.clear
        loadingImageLabel.textAlignment = .center
        dateBtn.setTitleColor(UIColor.white, for: .normal)
        dateBtn.setTitle(L.IN(ins:"Set date"), for: .normal)
        dateBtn.backgroundColor = Common.colorSecondary
        dateBtn.addTarget(self,
                          action: #selector(onSelectDate),
                          for: .touchUpInside
        )
        bookBtn.backgroundColor = Common.colorAccent
        bookBtn.layer.cornerRadius = Common.cornerRadius
        bookBtn.setTitleColor(UIColor.white, for: .normal)
        bookBtn.setTitle(L.IN(ins:"Book car :)"), for: .normal)
        bookBtn.addTarget(self,
                          action: #selector(onBook),
                          for: .touchUpInside
        )
        
        setDateLabelInitial()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func rearrangeViews() {
        
        var ypos = CGFloat(0.0)
        let lineHeight = CGFloat(60.0)
        let offset = Common.offset
        
        scrollView.frame = CGRect(x:0.0,y:0.0,width:self.view.frame.size.width,height:self.view.frame.size.height)
        carImage.frame = CGRect(x:0.0,y:0.0,width:self.view.frame.size.width,height:self.view.frame.size.width)
        nameLabel.frame = CGRect(x:0.0,y:0.0,width:carImage.frame.size.width,height:lineHeight)
        shortLabel.frame = CGRect(x:0.0,y:carImage.frame.size.width-lineHeight,width:carImage.frame.size.width,height:lineHeight)
        
        var descHeight = (carImage.frame.size.height/4.0)-20.0
        if detailShown == true {
            descHeight = (descriptionLabel.text?.height(withConstrainedWidth: carImage.frame.size.width, font: descriptionLabel.font))!
        }
        descriptionLabel.frame = CGRect(x:0.0,y:carImage.frame.origin.y+carImage.frame.size.height,width:carImage.frame.size.width,height:descHeight)
        
        let moreBtnWidth = CGFloat(50.0)
        let moreBtnHeight = CGFloat(30.0)
        moreBtn.frame = CGRect(x:descriptionLabel.frame.size.width-(moreBtnWidth+Common.offset),y:descriptionLabel.frame.origin.y+descriptionLabel.frame.size.height-moreBtnHeight-offset,width:moreBtnWidth,height:moreBtnHeight)
        loadingImageLabel.frame = self.carImage.frame
        
        ypos+=self.descriptionLabel.frame.origin.y+self.descriptionLabel.frame.size.height
        
        dateBtn.frame = CGRect(x:0.0,y:ypos,width:self.view.frame.size.width,height:lineHeight)
        ypos+=dateBtn.frame.size.height
        
        daysCountPicker.frame = CGRect(x:0.0,y:ypos,width:self.view.frame.size.width,height:lineHeight)
        ypos+=daysCountPicker.frame.size.height
        
        bookBtn.frame = CGRect(x:offset,y:ypos,width:self.view.frame.size.width-(2.0*offset),height:lineHeight-(2.0*offset))
        ypos+=bookBtn.frame.size.height
        
        ypos+=offset
        
        scrollView.contentSize = CGSize(width:self.view.frame.size.width,height:ypos)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        rearrangeViews()
    }
}
