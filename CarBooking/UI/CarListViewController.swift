//
//  CarListViewController.swift
//  CarBooking
//
//  Created by Milan Kazarka on 5/3/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class CarListViewController: CommonViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView : UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView?.register(CarListTableViewCell.self, forCellReuseIdentifier: CarListTableViewCell.cellIdentifier)
        
        NotificationCenter.default.addObserver(
            self, selector: #selector(self.dataChanged),
            name: NSNotification.Name(rawValue: Common.notifDataChanged),
            object: nil)
        Cars.sharedInstance.reload()
    }

    func dataChanged( ) {
        tableView?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Cars.sharedInstance.cars.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CarListTableViewCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : CarListTableViewCell! = nil
        cell = CarListTableViewCell(style:UITableViewCellStyle.default, reuseIdentifier: CarListTableViewCell.cellIdentifier)
        if let car = Cars.sharedInstance.carAtIndex(index: indexPath.row) as Car? {
            cell.loadCar(car:car)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"CarDetailsViewController") as! CarDetailsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        let car = Cars.sharedInstance.carAtIndex(index: indexPath.row)
        viewController.loadCar(car: car!)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
